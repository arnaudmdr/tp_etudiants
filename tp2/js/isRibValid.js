// --------------------------------------------------------------------------------------------------
// function isRibValid()
// calcul/vérification de la validité d'un RIB/RIP (Relevé d'Identité Bancaire/Postale)
// accepte 3 ou 4 arguments
// - 3 arguments :    code banque (numérique)
//                    code guichet (numérique)
//                    numéro de compte (alpha)
//                La fonction retourne alors la clé RIB Calculée
// - 4 arguments :    Clé RIB en plus (numérique)
//                La fonction retourne alors un booleen indiquant si le RIB est valide
//
// Attention : la validité des arguments (code bqe numérique, numéro de compte à 11 caractères, etc ...) n'est pas contrôlée par la fonction.
// --------------------------------------------------------------------------------------------------
function isRIBvalid()
{
if (isRIBvalid.arguments.length>=3)
    {
    var bqe=isRIBvalid.arguments[0];
    var gui=isRIBvalid.arguments[1];
    var cpt=isRIBvalid.arguments[2].toUpperCase();
    var tab= "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var tab1="123456789123456789234567890123456789".split("");
    while (cpt.match(/\D/) != null)
        cpt=cpt.replace(/\D/, tab1[tab.indexOf(cpt.match(/\D/))]);
    var cp=parseInt    (cpt, 10);
    
    a=bqe%97;
    a=a*100000+parseInt(gui, 10);
    a=a%97;
    a=a*Math.pow(10, 11) + cp;
    a=a%97
    a=a*100;
    a=a%97
    a=97-a;
    if (isRIBvalid.arguments.length>3)
        return isRIBvalid.arguments[3]==a;
    else
        return a;
    }
else
    {
    return false;
    }
}
