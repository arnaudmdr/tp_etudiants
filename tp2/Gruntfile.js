module.exports = function(grunt) {

    grunt.initConfig({

        //read the project config file
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
              // define a string to put between each file in the concatenated output
              separator: ';'
            },
            dist: {
              // the files to concatenate
              src: ['js/*.js','tests/*.js'],
              // the location of the resulting JS file
              dest: 'dist/<%= pkg.name %>.js'
            }
        },

        uglify: {
            options: {
              // the banner is inserted at the top of the output
              banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
            },
            dist: {
              files: {
                'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
              }
            }
        },

        qunit: {
            files: ['html/test.html']
          },

          qunit_junit: {
            options: {
                dest:'tmp/'
            }
        },

        watch: {
            options: {livereload: true},
            files: ['js/*.js','tests/*.js'],
            tasks: ['concat', 'uglify','qunit_junit', 'qunit']
        }


    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-qunit-junit');

    grunt.registerTask('default', ['concat', 'uglify','qunit_junit','qunit']);

};