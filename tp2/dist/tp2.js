
function chaineConcat(string) {
    return "Bonjour, " + string;
} 
;function clefNIR(assuranceCode) {
    return 97 - (parseInt(assuranceCode,10)%97);
};
function fibonacci(num) {
    if (num <= 1) return 1;
  
    return fibonacci(num - 1) + fibonacci(num - 2);
  };// --------------------------------------------------------------------------------------------------
// function isRibValid()
// calcul/vérification de la validité d'un RIB/RIP (Relevé d'Identité Bancaire/Postale)
// accepte 3 ou 4 arguments
// - 3 arguments :    code banque (numérique)
//                    code guichet (numérique)
//                    numéro de compte (alpha)
//                La fonction retourne alors la clé RIB Calculée
// - 4 arguments :    Clé RIB en plus (numérique)
//                La fonction retourne alors un booleen indiquant si le RIB est valide
//
// Attention : la validité des arguments (code bqe numérique, numéro de compte à 11 caractères, etc ...) n'est pas contrôlée par la fonction.
// --------------------------------------------------------------------------------------------------
function isRIBvalid()
{
if (isRIBvalid.arguments.length>=3)
    {
    var bqe=isRIBvalid.arguments[0];
    var gui=isRIBvalid.arguments[1];
    var cpt=isRIBvalid.arguments[2].toUpperCase();
    var tab= "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var tab1="123456789123456789234567890123456789".split("");
    while (cpt.match(/\D/) != null)
        cpt=cpt.replace(/\D/, tab1[tab.indexOf(cpt.match(/\D/))]);
    var cp=parseInt    (cpt, 10);
    
    a=bqe%97;
    a=a*100000+parseInt(gui, 10);
    a=a%97;
    a=a*Math.pow(10, 11) + cp;
    a=a%97
    a=a*100;
    a=a%97
    a=97-a;
    if (isRIBvalid.arguments.length>3)
        return isRIBvalid.arguments[3]==a;
    else
        return a;
    }
else
    {
    return false;
    }
}
;(function() {
    var nirRegex = /[12][ \.\-]?[0-9]{2}[ \.\-]?(0[1-9]|[1][0-2])[ \.\-]?([0-9]{2}|2A|2B)[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}/

    // assuranceCode must be a string
    function nirValidator(assuranceCode){          
        if(nirRegex.test(assuranceCode)){
            var nir = Number(
                assuranceCode
                .replace("2A","19")
                .replace("2B","18")
                .slice(0, assuranceCode.length - 2)
            );
            return ( 97 - nir % 97 ) == Number( assuranceCode.slice(-2) );
        } else {
            return false;
        }
    }
    
    // Establish the root object, `window` (`self`) in the browser, `global`
    // on the server, or `this` in some virtual machines. We use `self`
    // instead of `window` for `WebWorker` support.
    var root = typeof self === 'object' && self.self === self && self ||
            typeof global === 'object' && global.global === global && global ||
            this;

    
     // Node.js
    if (typeof module === 'object' && module.exports) {
        module.exports = nirValidator;
    }
    // AMD / RequireJS
    else if (typeof define === 'function' && define.amd) {
        define([], function () {
            return nirValidator;
        });
    }
    // included directly via <script> tag
    else {
        root.nirValidator = nirValidator;
    }
    
})();;function pair(number) {
    if (number%2==0) {
        return true;
    } else {
        return false;
    }
}


;function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};QUnit.test("test", function( assert ) {
    assert.equal(1,"1");
});


QUnit.test("testPair", function( assert ) {
  assert.expect(8);
  assert.ok(pair(0));
  assert.ok(pair(2));
  assert.ok(pair(4));
  assert.ok(pair(6));
  assert.ok(pair(18));
  assert.ok(pair(16));
  assert.ok(pair(20));
  assert.ok(pair(10));
});

QUnit.test("testConcat", function( assert ) {
  assert.expect(4);
  assert.equal("Bonjour, arnaud",chaineConcat("arnaud"));
  assert.equal("Bonjour, morgane",chaineConcat("morgane"));
  assert.equal("Bonjour, 4",chaineConcat(4));
  assert.equal("Bonjour, true",chaineConcat(true));
});

QUnit.test("testSecuriteSociale", function( assert ) {
  assert.expect(2);
  assert.ok(nirValidator("255081416802538"));
  assert.ok(nirValidator("177097645103210"));

});

QUnit.test("testRIB", function( assert ) {
  assert.expect(3);
  assert.ok(isRIBvalid("11111", "22222", "ABCD3333EFG", "42"));
  assert.ok(isRIBvalid("30002", "00550", "0000157845Z", "02"));
  assert.notOk(isRIBvalid("30002", "00110", "0000153845Z", "02"));
});

QUnit.test("testClefNir", function( assert ) {
  assert.expect(2);
  assert.equal(38, clefNIR("2550814168025"));
  assert.equal(10, clefNIR("1770976451032"));
});

QUnit.test("testFibonacci", function( assert ) {
  assert.equal(610,fibonacci(14));
  assert.equal(987,fibonacci(15));
});

QUnit.test("testEmail", function( assert ) {
  assert.ok(validateEmail("arnaud.bouix@insa-cvl.fr"));
  assert.ok(validateEmail("arnaud_b123@hotmail.fr"));
  assert.ok(validateEmail("arnaud_b123@hotmail.com"));
  assert.ok(validateEmail("arnaud_b123@hotmail.de"));
  assert.ok(validateEmail("arnaud_b123@hotmail.org"));
});








