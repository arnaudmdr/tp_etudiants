QUnit.test("test", function( assert ) {
    assert.equal(1,"1");
});


QUnit.test("testPair", function( assert ) {
  assert.expect(8);
  assert.ok(pair(0));
  assert.ok(pair(2));
  assert.ok(pair(4));
  assert.ok(pair(6));
  assert.ok(pair(18));
  assert.ok(pair(16));
  assert.ok(pair(20));
  assert.ok(pair(10));
});

QUnit.test("testConcat", function( assert ) {
  assert.expect(4);
  assert.equal("Bonjour, arnaud",chaineConcat("arnaud"));
  assert.equal("Bonjour, morgane",chaineConcat("morgane"));
  assert.equal("Bonjour, 4",chaineConcat(4));
  assert.equal("Bonjour, true",chaineConcat(true));
});

QUnit.test("testSecuriteSociale", function( assert ) {
  assert.expect(2);
  assert.ok(nirValidator("255081416802538"));
  assert.ok(nirValidator("177097645103210"));

});

QUnit.test("testRIB", function( assert ) {
  assert.expect(3);
  assert.ok(isRIBvalid("11111", "22222", "ABCD3333EFG", "42"));
  assert.ok(isRIBvalid("30002", "00550", "0000157845Z", "02"));
  assert.notOk(isRIBvalid("30002", "00110", "0000153845Z", "02"));
});

QUnit.test("testClefNir", function( assert ) {
  assert.expect(2);
  assert.equal(38, clefNIR("2550814168025"));
  assert.equal(10, clefNIR("1770976451032"));
});

QUnit.test("testFibonacci", function( assert ) {
  assert.equal(610,fibonacci(14));
  assert.equal(987,fibonacci(15));
});

QUnit.test("testEmail", function( assert ) {
  assert.ok(validateEmail("arnaud.bouix@insa-cvl.fr"));
  assert.ok(validateEmail("arnaud_b123@hotmail.fr"));
  assert.ok(validateEmail("arnaud_b123@hotmail.com"));
  assert.ok(validateEmail("arnaud_b123@hotmail.de"));
  assert.ok(validateEmail("arnaud_b123@hotmail.org"));
});








