module.exports = function(grunt) {
    
        grunt.initConfig({
    
            //read the project config file
            pkg: grunt.file.readJSON('package.json'),

            sprite:{
                all: {
                  src: 'img/*.png',
                  dest: 'sprite/spritesheet.png',
                  destCss: 'css/sprites.css'
                }
              }
    
    
        });
    
        grunt.loadNpmTasks('grunt-spritesmith');
    
        grunt.registerTask('default', ['sprite']);
    
    };