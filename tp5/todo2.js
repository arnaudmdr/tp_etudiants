describe('angularjs homepage todo list', function() {
    it('should add a todo', function() {
      browser.get('http://todomvc.com/examples/react/#/');
  
      element(by.css('.new-todo')).sendKeys('write first protractor test');
      element(by.css('.new-todo')).sendKeys(protractor.Key.ENTER);
  
      //Check list 1 item
      var todoList = element.all(by.css('.todo-list'));
      expect(todoList.count()).toEqual(1);

      //Check if one item left
      var todoCount = element(by.css('.todo-count'));
      expect(todoCount.getText()).toEqual("1 item left");

      //Effectuer la tache
      element(by.css('.toggle')).click();

      //Verifier qu'elle est terminée (on pourrait faire autrement), on aurait pu aller voir dans completed pour voir si elle était faite
      var todoCount = element(by.css('.todo-count'));
      expect(todoCount.getText()).toEqual("0 items left");

      //Supprimer elt
      element(by.css('.destroy')).click();

      //Check if list vide
      var todoList = element.all(by.css('.todo-list'));
      expect(todoList.count()).toEqual(0);


    });
  });