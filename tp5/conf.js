exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['google.js'],
    capabilities: {
        'browserName':'firefox'
    },
    onPrepare: function() {
        browser.ignoreSynchronization = true;
    }
  };