<?php 
  class Poneys {
      private $count = 8;

      public function getCount() {
        return $this->count;
      }

      public function setCount($number) {
        if ($number < 0) {
          throw new InvalidNumberException();
        } else {
          $this->count = $number;
        }
      }

      public function removePoneyFromField($number) {
        if ((($this->count - $number) < 0)) {
          throw new TooMuchPoneyRemoveException();
        } else if (($number < 0)) {
          throw new InvalidNumberException();
        } else {
          $this->count -= $number;
        }       
      }

      public function addPoneyInField($number) {
        $this->count += $number;
      }

      //Returne true si il y a moins de 15 moutons
      public function isChampLibre() {
        return ($this->count < 15);
      }

      public function getNames() {

      }


  }


  //Exceptions
  class TooMuchPoneyRemoveException extends Exception {

    public function __construct() {
      parent::__construct();
    }
    
    public function __toString() {
      return "mdr";
    }
  }

  class InvalidNumberException extends Exception {

    public function __construct() {
      parent::__construct();
    }
    
    public function __toString() {
      return "mdr";
    }
  }


?>
