<?php
  require_once 'src/Poneys.php';
  use PHPUnit\Framework\TestCase;

  class PoneysTest extends TestCase {

    protected $Poneys;

    public function setUp() {
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(QUANTITE_PONEYS);
    }

    public function tearDown() {
      $this->Poneys = null;
    }


    public function test_removePoneyFromField() {
      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());

    }

    public function test_addPoneyInField() {

      // Action
      $this->Poneys->addPoneyInField(3);
      
      // Assert
      $this->assertEquals(11, $this->Poneys->getCount());
    }

    /**
     * @expectedException TooMuchPoneyRemoveException
     */
    public function test_tooMuchException() {

      // Action
      $this->Poneys->removePoneyFromField(9);
    }

    /**
     * @expectedException InvalidNumberException
     */
    public function test_invalidNumberException() {

      // Action
      $this->Poneys->removePoneyFromField(-3);
    }

  
    public function poneyProvider() {
      return [
        [2,6],
        [4,4],
        [6,2]
      ];
    } 


    /**
     * @dataProvider poneyProvider
     * 
     */
    public function testPoneyProvider($poney, $total) {
       
      // Action
      $this->Poneys->removePoneyFromField($poney);
      $this->assertEquals($total, $this->Poneys->getCount());

    }


    public function testMockGetNames() {
 
      //Recupere le mock Poneys
      $this->names = $this->getMockBuilder('Poneys')->getMock();

      //La methode doit renvoyer exactement une fois les valeurs...
      $this->names->expects($this->exactly(1))
            ->method('getNames')
            ->willReturn(["Ponet","Poné","Peauner","Paunai"]);

      $this->assertEquals(
        ["Ponet","Poné","Peauner","Paunai"],
        $this->names->getNames()
      );

    }

    public function testMockProphecy() {
      $this->prophet = new \Prophecy\Prophet;


      $this->poneys = $this->prophet->prophesize('Poneys');

      $this->poneys->getNames()->willReturn(["Ponet","Poné","Peauner","Paunai"]);

      /*
      $this->assertEquals(
        ["Ponet","Poné","Peauner","Paunai"],
        $this->poneys->getNames()
      );
      */

    }


    public function test_isChampLibre() {

      // Action
      $this->Poneys->addPoneyInField(3);
      $this->assertTrue($this->Poneys->isChampLibre());

    }

    public function test_isChampPlein() {
  
      // Action
      $this->Poneys->addPoneyInField(7);
      $this->assertFalse($this->Poneys->isChampLibre());

    }

    public function test_setCount() {
      //Action
      $this->Poneys->setCount(7);
      $this->assertEquals(7,$this->Poneys->getCount());
    }


    /**
     * @expectedException InvalidNumberException
     */
    public function test_setCountException() {
      //Action
      $this->Poneys->setCount(-7);
    }
    
  





  }

  
 ?>
